package com.example.quizapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Connexion extends AppCompatActivity {

    EditText login;
    EditText password;
    Button btnLogin;

    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);

        login = findViewById(R.id.login);
        password = findViewById(R.id.password);
        btnLogin = findViewById(R.id.connecter_btn);

        mAuth = FirebaseAuth.getInstance();
        btnLogin.setOnClickListener(view -> {
            loginUser();
        });


    }

    private void loginUser(){
        String email = login.getText().toString();
        String mdp = password.getText().toString();

        if(TextUtils.isEmpty(email)){
            login.setError("Email cannot be empty");
            login.requestFocus();

        }
        else if (TextUtils.isEmpty(mdp)){
            password.setError("password cannot be empty");
            password.requestFocus();
        }else{
            mAuth.signInWithEmailAndPassword(email,mdp).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(Connexion.this,"User is Logged succesfully",Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(Connexion.this,MainActivity.class));
                    }
                    else{
                        Toast.makeText(Connexion.this,"Log in Error:"+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}
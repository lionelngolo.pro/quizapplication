package com.example.quizapplication;

public class QuestionAnswer {

    public static String question[] ={
            "Quelle est le navigateur web ?",
            "Lequel n'est pas un langage de programmation?",
            "Lequel n'est pas un réseau social ?",
            "Quel est la compagnie qui produit des Iphone ?",
            " 1 + 25 + 10 = ?",
            " 10 / 2 = ?"
    };

    public static String choices[][] = {
            {"Google","Apple","Nokia","Samsung"},
            {"Java","Kotlin","GitHub","Python"},
            {"Facebook","Snapchat","Instagram","Youtube"},
            {"Google","Apple","Nokia","Samsung"},
            {"0","35","46","36"},
            {"10","50","15","5"},

    };

    public static String correctAnswers[] = {
            "Google",
            "GitHub",
            "Youtube",
            "Apple",
            "36",
            "5"
    };



}

package com.example.quizapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ListeAmi extends AppCompatActivity {

    private FirebaseFirestore db;
    private String idToReturn;

    FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_ami);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        ListView listView = findViewById(R.id.listView);

        List<String> list = new ArrayList<>();

        db.collection("users")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("email 1",mAuth.getCurrentUser().getEmail());
                                Log.d("email 2",document.getData().get("email").toString());
                                if(mAuth.getCurrentUser().getEmail().toString().equals(document.getData().get("email").toString()) ){
                                    Log.d("email and id :", document.getId());
                                    db.collection("users").document(document.getId()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                        @Override
                                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                                            Log.d("result liste amis", documentSnapshot.getId() + " => " + documentSnapshot.getData().get("ami1") +" et " +documentSnapshot.getData().get("ami2"));
                                            //amis
                                            db.collection("users").document(documentSnapshot.getData().get("ami1").toString()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                                @Override
                                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                                    Log.d("result instance",mAuth.getCurrentUser().getEmail());
                                                    Log.d("result", documentSnapshot.getId() + " => " + documentSnapshot.getData().get("score"));
                                                    list.add(documentSnapshot.getData().get("prenom").toString()+" | Score :"+documentSnapshot.getData().get("score").toString()+" ");
                                                }
                                            }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    Log.d("fail", "failed");

                                                }
                                            });

                                            db.collection("users").document(documentSnapshot.getData().get("ami2").toString()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                                @Override
                                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                                    Log.d("result instance",mAuth.getCurrentUser().getEmail());
                                                    Log.d("result", documentSnapshot.getId() + " => " + documentSnapshot.getData().get("score"));
                                                    list.add(documentSnapshot.getData().get("prenom").toString()+" | Score :"+documentSnapshot.getData().get("score").toString()+" ");
                                                    ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_expandable_list_item_1,list);
                                                    listView.setAdapter(arrayAdapter);

                                                }
                                            }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    Log.d("fail", "failed");

                                                }
                                            });


                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.d("fail", "failed");

                                        }
                                    });
                                }
                                Log.d("test success", document.getId() + " => " + document.getData());
                            }
                        } else {
                            Log.w("test fail :", "Error getting documents.", task.getException());
                        }
                    }
                });






           /* list.add("User 1 | Score : ");
            list.add("User 2 | Score : ");*/




        BottomNavigationView bottomNavigationView=findViewById(R.id.bottom_navigation);

        bottomNavigationView.setSelectedItemId(R.id.ami);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch(item.getItemId())
                {
                    case R.id.home:
                        startActivity(new Intent(getApplicationContext(),MainActivity.class));
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.ami:
                        return true;
                    case R.id.quiz:
                        startActivity(new Intent(getApplicationContext(),Question.class));
                        overridePendingTransition(0,0);
                        return true;
                }
                return false;
            }
        });


    }

    void load(String id){
        db.collection("users").document(id).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Log.d("result", documentSnapshot.getId() + " => " + documentSnapshot.getData().get("listeAmis"));
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("fail", "failed");

            }
        });
    }

    void getIdUser(){
        String email = mAuth.getCurrentUser().getEmail();

        db.collection("users")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                if(mAuth.getCurrentUser().getEmail() == document.getData().get("email")){
                                    Log.d("email and id :", document.getId());
                                    idToReturn = document.getId();
                                }else{
                                    Log.w("test fail :", "Error getting documents.", task.getException());
                                }
                                Log.d("test success", document.getId() + " => " + document.getData());
                            }
                        } else {
                            Log.w("test fail :", "Error getting documents.", task.getException());
                        }
                    }
                });



    }

    void showFriends(){

    }

}